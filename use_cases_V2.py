"""
August 2018
Emma Freeman

pull order data from Looker
run functions to tag/categorize orders
upload to S3
import to Redshift
"""

import pandas as pd
import numpy as np
import re
import json
import requests
import configparser
from math import radians, cos, sin, asin, sqrt
import boto3

#Read config file with Looker API and Database connection information
config = configparser.ConfigParser(allow_no_value=True)
config.read('config')

#Very Basic Looker API class allowing us to access the data from a given Look ID
class lookerAPIClient:
    def __init__(self, api_host=None, api_client_id=None, api_secret=None, api_port='19999'):
        auth_request_payload = {'client_id': api_client_id, 'client_secret': api_secret}
        self.host = api_host
        self.uri_stub = '/api/3.0/'
        self.uri_full = ''.join([api_host, ':', api_port, self.uri_stub])
        response = requests.post(self.uri_full + 'login', params=auth_request_payload)
        authData = response.json()
        self.access_token = authData['access_token']
        self.auth_headers = {
                'Authorization' : 'token ' + self.access_token,
                }

    def post(self, call='', json_payload=None):
        response = requests.post(self.uri_full + call, headers=self.auth_headers, json=json_payload)
        return response.json()

    def get(self, call=''):
        response = requests.get(self.uri_full + call, headers=self.auth_headers)
        return json.loads(response.content) #response.json()

    def runLook(self, look, limit):
        optional_arguments = '?' + 'limit=' + str(limit)
        return self.get('/'.join(['looks',look,'run','json'])+optional_arguments)

#Initialize the Looker API Class with the data in our config file (which is stored in a neighboring file 'config')
x = lookerAPIClient(
        api_host      = config.get('api', 'api_host'), 
        api_client_id = config.get('api', 'api_client_id'), 
        api_secret    = config.get('api', 'api_secret'), 
        api_port      = config.get('api', 'api_port')
        )   


# Use the API to get data from the Look - filter Look first for correct date range
orders = x.runLook(look='1427', limit=1000000)

df = pd.DataFrame(orders)

numcols = ['jobs.lat', 'jobs.lon']
catcols = ['jobs.user_id', 'jobs.order_id', 'users.country_id', 'jobs.id', 'jobs.card_address']
colnames = ['card_address','job_id', 'pic_lat', 'pic_lon', 'order_id', 'product_type', 'template_uuid', 'user_id', 'message', 'created_date', 'postcode_UK', 'postcode_US', 'country']

def format_dataframe(df, numcols, catcols, colnames):

    # replace zeros with NaN for lat and long
    df[numcols] = df[numcols].replace(0, np.nan)
      
    # change data type to string for categorical columns
    df[catcols] = df[catcols].astype(str)
    
    # set column header names
    df = df.set_axis(colnames, axis=1, inplace=False)

    return df


def parse_message_text(text):
    s = json.loads(text)
    message = ''
    if s is not None:
        for chunk in s:
            try:
                message += chunk['text'] + ' '
            except:
                continue
        message = re.sub(r"[-]+\ *", " ", message)
        message = re.sub(r"\bu\b", "you", message)
        message = message.replace('\n', ' ').replace('\r', ' ').replace(',', ' ').lower().strip()
    return message


def format_postcodes(df):

    # format UK postcodes
    df['postcode_UK'] = df['postcode_UK'].apply(lambda x: x.replace(' ', '') if pd.notnull(x) else x)
    df['postcode_UK'] = df['postcode_UK'].apply(lambda x: x[:-3] if pd.notnull(x) else x)

    # format US postcodes
    df['postcode_US'] = df['postcode_US'].apply(lambda x: x[:5] if pd.notnull(x) else x)
    
    # combine postcode columns
    df['postcode'] = df['postcode_UK'].combine(df['postcode_US'], lambda x1, x2: x1 if pd.notnull(x1) else x2)
    
    df['postcode'] = df['postcode'].astype(str)
    
    df.drop(['postcode_UK', 'postcode_US'], axis=1, inplace=True)
    
    return df


def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points 
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians 
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a)) 
    r = 6371 # Radius of earth in kilometers. Use 3956 for miles
    return c * r

# set a distance threshold (in km) between pic and home for classifying as 'traveling'
threshold = 200

travel_words = re.compile("(?<!you are )(?<!you're )(?<!your )(having a \\w+ time|\\btrip\\b|\\bholiday\\b|vacation|abroad)|(hello|greetings) from |sightseeing|view from|hiked")
thank_you_words = ['thank you', 'thanks for', 'thanks so much', 'many thanks']
bulk_words = ['https', 'www.', '.com', '.org']
thinking_words = ['just wanted to', 'just want to', 'just to', 'just dropping', 'thinking of you', 'a note', 'to say hi']
not_thinking_words = ['thank you for', 'thank you so much for']
relatives_words = re.compile('cousin[s]*|aunt[A-Za-z]*|uncle')
grandparent_words = re.compile('grand[a-z]+|gram[a-z]+|granny |\\bnan+a\\b|poppa|papa')
parent_words = re.compile('\\bmom[ma|my]*\\b|mama|\\bmum[my]*\\b|pap+a|poppa|dada|\\bdad[dy]*\\b')
father_mother_day = re.compile("(father|mother)+'*s+'* day")
hanukkah_words = ['hanukkah','chanukah', 'hanukah', 'hannukah', 'chanuka', 'chanukkah', 'hanuka', 'channukah', 'chanukka', 'hanukka', 'hannuka', 'hannukkah', 'channuka', 'xanuka', 'hannukka', 'channukkah', 'channukka','chanuqa']
religion_words = ['congregation', 'scripture','scriptural','baptism', 'communion', 'faith', 'blessing', '\bgod\b', 'jesus', 'bible', 'godmother', 'godfather', 'godson', 'goddaughter', 'the lord', 'our lord', 'at church', 'our church', 'from church', 'prayer', 'praying']
xmas_words = re.compile('(happy|merry) (christmas|xmas)')
    
def bday_words_search(text):
    regex = re.compile("(happy |your )([0-9]*)(th |nd |st )*(?<!my )(birthday|bday)") # FIX THIS!
    if regex.search(text):
        return 1
    else:
        return 0
    

def find_thinking_words(text, thinking_words=thinking_words, not_thinking_words=not_thinking_words):
    if (any(word in text for word in thinking_words) and
        not any(word in text for word in not_thinking_words)):
        return 1
    else:
        return 0
    

def baby_words_search(text):
    regex = re.compile("([0-9]+ *)(lbs|pounds) *([0-9]+ *)(oz|ounces) *|(born on)|(?<!in )([0-9]+) (month)+")
    unregex = re.compile("passed away|memorial service")
    if regex.search(text) and not unregex.search(text):
        return 1
    else:
        return 0

latlongs = pd.read_pickle('data/latlongs')

def apply_fxns(df, latlongs):
    df_clean = format_dataframe(df, numcols, catcols, colnames)
    df_clean['clean_message'] = df_clean['message'].apply(parse_message_text)
    df_clean.drop(df_clean[df_clean['clean_message']==''].index, inplace=True)
    df_clean.drop('message', axis=1, inplace=True)
    df_clean = format_postcodes(df_clean)
    df_clean = pd.merge(df_clean, latlongs, how='left', on='postcode')
    df_clean['distance_km'] = df_clean.apply(lambda x: haversine(x['home_lon'], x['home_lat'], x['pic_lon'], x['pic_lat']), axis=1)
    df_clean['travel_distance'] = df_clean['distance_km'].apply(lambda x: 1 if x > threshold else 0)
    df_clean['holiday_template'] = df_clean[pd.notnull(df_clean['template_uuid'])]['template_uuid'].apply(lambda x: 1 if 'XMAS' in x else 0)
    df_clean['thanks_template'] = df_clean[pd.notnull(df_clean['template_uuid'])]['template_uuid'].apply(lambda x: 1 if 'THANKYOU' in x else 0)
    df_clean['mothers_template'] = df_clean[pd.notnull(df_clean['template_uuid'])]['template_uuid'].apply(lambda x: 1 if 'MOTHER' in x else 0)
    df_clean[['holiday_template', 'thanks_template', 'mothers_template']] = df_clean[['holiday_template', 'thanks_template', 'mothers_template']].fillna(0)
    df_clean['sent_to_camp'] = df_clean['card_address'].apply(lambda x: 1 if re.search("\\bcamp\\b", x) else 0)
    df_clean['travel_words'] = df_clean['clean_message'].apply(lambda x: 1 if re.search(travel_words, x) else 0)
    df_clean['bulk_words'] = df_clean['clean_message'].apply(lambda x: 1 if any(word in x for word in bulk_words) else 0)
    df_clean['thank_you'] = df_clean.apply(lambda x: 1 if (any(word in x['clean_message'] for word in thank_you_words)) &
                                                            (x['bulk_words']==0) else 0, axis=1)
    df_clean['jobs_per_order'] = df_clean.groupby('order_id')['job_id'].transform('count')
    df_clean['bday'] = df_clean['clean_message'].apply(bday_words_search)
    df_clean['thinking_words'] = df_clean['clean_message'].apply(find_thinking_words)
    df_clean['relatives_words'] = df_clean['clean_message'].apply(lambda x: 1 if re.search(relatives_words, x) else 0)
    df_clean['from_gparent'] = df_clean['clean_message'].apply(lambda x: 1 if re.search(grandparent_words, x[-20:]) else 0)
    df_clean['from_parent'] = df_clean['clean_message'].apply(lambda x: 1 if re.search(parent_words, x[-20:]) else 0)
    df_clean['to_gparent'] = df_clean['clean_message'].apply(lambda x: 1 if re.search(grandparent_words, x[:20]) else 0)
    df_clean['to_parent'] = df_clean['clean_message'].apply(lambda x: 1 if re.search(parent_words, x[:20]) else 0)
    df_clean['parent_words'] = df_clean['clean_message'].apply(lambda x: 1 if re.search(parent_words, x) else 0)
    df_clean['baby_words'] = df_clean['clean_message'].apply(baby_words_search)
    df_clean['fathers_mothers_day'] = df_clean['clean_message'].apply(lambda x: 1 if re.search(father_mother_day, x) else 0)
    df_clean['new_years'] = df_clean['clean_message'].apply(lambda x: 1 if "happy new year" in x else 0)
    df_clean['halloween'] = df_clean['clean_message'].apply(lambda x: 1 if "halloween" in x else 0)
    df_clean['thanksgiving'] = df_clean['clean_message'].apply(lambda x: 1 if "thanksgiving" in x else 0)
    df_clean['easter'] = df_clean['clean_message'].apply(lambda x: 1 if "easter" in x else 0)
    df_clean['valentines'] = df_clean['clean_message'].apply(lambda x: 1 if re.search("valentine'*s day", x) else 0)
    df_clean['christmas'] = df_clean['clean_message'].apply(lambda x: 1 if re.search(xmas_words, x) else 0)
    df_clean['hanukkah'] = df_clean['clean_message'].apply(lambda x: 1 if any(word in x for word in hanukkah_words) else 0)
    df_clean['misc_holiday'] = df_clean['clean_message'].apply(lambda x: 1 if "happy holidays" in x else 0)
    df_clean['religion'] = df_clean['clean_message'].apply(lambda x: 1 if any(word in x for word in religion_words) else 0)
    df_clean['to_inmate'] = df_clean.apply(lambda x: 1 if (any(word in x['clean_message'] for word in ['when you get out\b', 'until you get out\b'])|(any(word in x['card_address'] for word in ['jail','prison', 'correctional']))) else 0, axis=1)

    
    df_clean = df_clean[['order_id', 'job_id', 'user_id', 'clean_message', 'jobs_per_order', 'travel_distance','holiday_template', 'thanks_template', 'mothers_template', 'travel_words', 'thank_you', 'bulk_words', 'bday', 'thinking_words', 'sent_to_camp', 'relatives_words', 'parent_words', 'to_gparent', 'to_parent', 'from_gparent', 'from_parent', 'baby_words', 'fathers_mothers_day','new_years', 'halloween', 'thanksgiving', 'easter', 'valentines','christmas','hanukkah', 'misc_holiday', 'religion', 'to_inmate']]
    df_clean['sum_use_cases'] = df_clean.iloc[:,9:].sum(axis=1)
    
    return df_clean


df_clean = apply_fxns(df, latlongs)

# upload to S3
s3 = boto3.client('s3')
s3.upload
# 