
# Pull data from Looker, use to find: 
 * habit moment (correlated with long-term retention)
 * aha moment (correlated with habit moment)
 * setup moment (correlated with aha moment)


V5 change from V4: 
 - *don't* filter by users with more than 1 order and more than 10 GBP spent (this prematurely filters out users)
 - base on first download in 2016 or after, and first order date between 2017-01-01 and 2017-11-01



**Inputs: **

1) 'retention_user_facts.csv' https://touchnote.eu.looker.com/looks/1866
 * days since last order (indicates whether user is retained or not)
 * first download, signup, order, and payment dates
 
2) 'retention_user_actions.csv' https://touchnote.eu.looker.com/looks/1855
 * per user by month since signup
 * orders, recipients, etc.
 
3) 'retention_count_items_orders_30_days.csv' https://touchnote.eu.looker.com/looks/1902?toggle=dat,fil,pik,vis
* in 30 days since signup, how many orders and items
 
4) 'first_main_count_use_cases.csv'
 * 'first_use_case', 'main_use_case', 'number_of_use_cases'
 
5) 'retention_orders_first_year.csv' https://touchnote.eu.looker.com/looks/1900
 * months since first order, orders (includes first order in count; used to get habit moment of 3 quarters ordered in first year)
 
6) 'credits_bought.csv' 
* credits bought in first 6 months after user creation/signup

7) 'year_total_orders_days.csv'
* total orders and days ordered in first year
 

DIDN'T/CAN'T RUN, BUT NEED:
 
 'retention_user_events_days.csv' https://touchnote.eu.looker.com/looks/1883
 * per user, app events, by days since signup
 * 'unique_sessions', events 
 
 'retention_user_events_hours.csv' https://touchnote.eu.looker.com/looks/1882
 * per user, app events w/in 24 hours of download and signup
 
 - some inputs around other specific in-app events, like opened app, viewed certain screens, etc.
     
**Outputs: **
 * retention_corrs_by_use_case.csv
 
**Results: **
 * habit moment: users who ordered in 3 or 4 quarters in their first year (depends on the use case), and slight correlation with 3 unique use cases in their first year
 * aha moment: nothing in current dataset that correlates with the aha moment
 * setup moment: nothing in current dataset that correlates with the aha moment
